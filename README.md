# Linux 102


## How to log in to ubuntu 

`~ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98`

## How to create users in Linux (ubutu)

To add user : `sudo adduser <username>`

You are promted to give password. __(123) for both users__

## How to delete usesrs

To delete user : `sudo deluser <username>`

## How to switch to user

To switch user: `su - <username>`

## How to list all users

List users : `getent passwd`

## How to get sudo powers

Give sudo privileges : `usermod -aG sudo <username>`

If above doesn't work : `sudo usermod -aG sudo <username>`

Check if user is assigned to sudo group: `groups <username>`

This will show what group the user is in.

## Give user authorization keys

To be able to login directily to your user in the virtual machine.

1. Get the authorized_keys
   
   - `cd .ssh`
   - `cat authorized_keys`
   - copy the key

2. Go to your user
   
   - `su - <username>`
   
3. Create `.ssh` folder and authorized_keys file
   
   - `mkdir .ssh`
   - `cd .ssh`
   - `touch authorized_keys`
  

4. Add the key
   
    - `nano ~/.ssh/authorized_keys`
    - paste the key and save
  
5. Check this works by logging out of the virtual machine and logging direclty into your user
   
   - `~ ssh -i ~/.ssh/ch9_shared.pem ,<username>@34.245.2.98`
  
    This will log you in to the virtual machine on your user.

## what is sudo